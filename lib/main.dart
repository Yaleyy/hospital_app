import 'package:flutter/material.dart';
import 'package:hospital_new_app/pages/auth_page.dart';
import 'package:hospital_new_app/pages/home_page.dart';
import 'package:hospital_new_app/pages/onboard_pages.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
      routes: {
        '/onboard_pages' : (context) => OnboardPages(),
        '/auth' : (context) => AuthPage(),
      },
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
    );
  }
}
