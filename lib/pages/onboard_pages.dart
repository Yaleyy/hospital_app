import 'package:flutter/material.dart';
import 'package:hospital_new_app/pages/onboard_one.dart';
import 'package:hospital_new_app/pages/onboard_three.dart';
import 'package:hospital_new_app/pages/onboard_two.dart';

class OnboardPages extends StatelessWidget {
  const OnboardPages({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: PageView(
          children: [
            OnboardOne(),
            OnboardTwo(),
            OnboardThree()
          ],
        ),
      ),
    );
  }
}
