import 'package:flutter/material.dart';
import 'dart:async';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    Timer(Duration(seconds: 2), (){
      Navigator.pushReplacementNamed(context, '/onboard_pages');
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            stops: [
              0.1,
              0.5,
              0.6,
              1
            ],
            colors: [
              Color(0xff74C8E4),
              Color(0xff252dd3),
              Color(0xff252dd3),
              Color(0xffA1CAFF)
            ],
          ),
        ),
        child: Center(
          child: Image.asset('assets/logo/logo.png'),
        ),
      ),
    );
  }
}
