import 'package:flutter/material.dart';

class OnboardThree extends StatelessWidget {
  const OnboardThree({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.symmetric(
            horizontal: 30
        ),
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            const SizedBox(height: 50),
            Row(
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/auth');
                  },
                  child: const Text(
                      'Завершить',
                      style: TextStyle(
                        fontFamily: 'lato',
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                      )),
                ),
              ],
            ),
            const SizedBox(height: 199),
            Container(
              height: 93,
              width: 217,
              child: const Column(
                children: [
                  Text(
                    'Мониторинг',
                    style: TextStyle(
                      fontFamily: 'lato',
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff00B712),
                    ),
                  ),
                  SizedBox(height: 29),
                  Text(
                    'Наши врачи всегда наблюдают\nза вашими показателями здоровья',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'SF Pro',
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff939396),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 60),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleWidget(color: Colors.transparent),
                CircleWidget(color: Colors.transparent),
                CircleWidget(color: Color(0xff57A9FF)),
              ],
            ),
            Expanded(child: SizedBox()),
            Container(
              height: 269,
              width: 359,
              child: Image.asset('assets/ilustration/onboard_three.png'),
            ),
            SizedBox(height: 85),
          ],
        ),
      ),
    );
  }
}

class CircleWidget extends StatelessWidget {
  Color color;
  CircleWidget({
    super.key, required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 9),
      height: 12,
      width: 12,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xff57A9FF),
        ),
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}
